<?php

	function adm_check()
	{
		global $time;
	
		if (!isset($_SESSION['adm_auth']) or $_SESSION['adm_auth'] < $time)
		exit(header('Location: '.APANEL.'/?go='.base64_encode($_SERVER['REQUEST_URI']).'&'.passgen().'&'.SID));
	
		if (isset($_SESSION['adm_auth']) AND $_SESSION['adm_auth'] > $time)
		$_SESSION['adm_auth'] = $time + 1200;
	}

?>