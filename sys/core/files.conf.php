<?php
/*
	--Содержание файла--
	1. Время нового файла
	2. Определение разрещения файла
	3. Копирайт изображения
	4. Вычесление обьема файлов
	5. Сортировка массива загрузок
*/
	// 1. Время нового файла \\
	function file_new($file=NULL)
	{
	global $set;
	if (is_file($file) && filectime($file)>time()-60*60)
		return ' <span class="off"><b> new!!!</b></span>';
	elseif (is_file($file) && filectime($file)>time()-60*60*$set['loads_new_file_hour'])
		return ' <span class="off"> new</span>';
	}
	
	#@old
	function k_files($dir=null)
	{
	global $set;
		if ($dir!=null)
		{
			$path=(function_exists('iconv'))?iconv('windows-1251', 'utf-8', $dir):$dir;
			$path='/'.preg_replace('#^/+|/+$#i', null, $path).'/';
			$all= mysql_result(query("SELECT COUNT(*) FROM `loads_list` WHERE `path` LIKE '".my_esc($path)."%'"),0);
			$new= mysql_result(query("SELECT COUNT(*) FROM `loads_list` WHERE `path` LIKE '".my_esc($path)."%' AND `time` > '".(time()-60*60*$set['loads_new_file_hour'])."'"),0);
		}
		else
		{
			$all= mysql_result(query("SELECT COUNT(*) FROM `loads_list`"),0);
			$new= mysql_result(query("SELECT COUNT(*) FROM `loads_list` WHERE `time` > '".(time()-60*60*$set['loads_new_file_hour'])."'"),0);
		}

	return ($new==0)?$all:''.$all.'/<span class="new">+'.$new.'</span>';
	}
	// 2. Определение разрещения файла \\
	function ras_to_mime($ras=null)
	{
		if ($ras==null)
		{
		return 'application/octet-stream';
		}
		else
		{
			$htaccess=file(H.'.htaccess');
			for ($i=0;$i<count($htaccess);$i++)
			{
				if (preg_match('#^AddType#i',trim($htaccess[$i])))
				{
				$type=explode(' ', trim($htaccess[$i]));
				$rass=str_replace('.',null,$type[2]);
				$mime[$rass]=$type[1];
				}
			}

			if (isset($mime[$ras])) {return $mime[$ras];}
			else
				return 'application/octet-stream';
		}
	}
	
	// 3. Копирайт изображения \\
	// накладываем копирайт на изображения, если он есть. \\
	// размер копирайта динамически изменяется в зависимости от изображения \\
	
	function img_copyright($img)
	{
	global $set;
		if (isset($set['copy_path']) && $set['copy_path']!=null && $copy=@imagecreatefromstring(file_get_contents(H.$set['copy_path'])))
		{

		$img_x=imagesx($img);
		$img_y=imagesy($img);
		
		$copy_x=imagesx($copy);
		$copy_y=imagesy($copy);
		
		$w=intval(min($img_x/2.5,$copy_x,128));
		$h=intval(min($img_y/2.5,$copy_y,64));
		
		$x_ratio = $w/$copy_x; 
		$y_ratio = $h/$copy_y; 
			if (($copy_x <= $w) && ($img_y <= $h))
			{
			$dstW = $copy_x;
			$dstH = $copy_y;
			} 
			elseif (($x_ratio * $copy_y) < $h)
			{ 
			$dstH = ceil($x_ratio * $copy_y);
			$dstW = $w;
			}
			else
			{
			$dstW = ceil($y_ratio * $copy_x);
			$dstH = $h;
			}
			imagecopyresampled($img, $copy, $img_x-$dstW, $img_y-$dstH, 0, 0, $dstW, $dstH, $copy_x, $copy_y);

		}
	return $img;
	}
	// 4. Вычесление обьема файлов \\
	function size_file($filesize = 0)
	{
	$filesize_ed = 'B';
		if ($filesize>=1024){$filesize= round($filesize/1024 , 2);$filesize_ed='KB';}
		if ($filesize>=1024){$filesize= round($filesize/1024 , 2);$filesize_ed='MB';}
		if ($filesize>=1024){$filesize= round($filesize/1024 , 2);$filesize_ed='GB';}
		if ($filesize>=1024){$filesize= round($filesize/1024 , 2);$filesize_ed='TB';}
	return $filesize.$filesize_ed;
	}

	// 5. Сортировка массива загрузок \\
	function cmp2 ($a, $b) {
		if ($a['2'] == $b['2']) return 0;
    return ($a['2'] > $b['2']) ? -1 : 1;
	}
?>